from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from .forms import TodoListForm, EditPageForm, TodoItemForm

def todo_list(request):
    todolist = TodoList.objects.all()
    context = {
        "todo_list": todolist,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    todo_items = TodoItem.objects.filter(list=todolist)
    context = {
        "todo_object": todolist,
        "todo_list_detail": todo_items,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", todo_list.id)
    else:
            form = TodoListForm()

    context = {
            "form" : form,}
    return render(request, "todos/create.html", context)



def edit_page(request, id):
    todolist = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
          form = EditPageForm(request.POST, instance=todolist)
          if form.is_valid():
               form.save()
               return redirect("todo_list_detail", id=todolist.id)
    else:
        form = EditPageForm(instance=todolist)

    context = {
         "form": form,
         "todo_list": todolist
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todolist = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")

    context = {"todo_list": todolist}
    return render(request, "todos/delete.html", context)


def todo_item_create(request):
    todo_lists = TodoList.objects.all()

    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm()

    context = {"form": form, "todo_lists": todo_lists}
    return render(request, "todos/item_create.html", context)


def todo_item_update(request, id):
    todo_lists = TodoList.objects.all()

    todo_item = get_object_or_404(TodoItem, id=id)

    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)

    context = {"form": form, "todo_item": todo_item, "todo_lists": todo_lists}
    return render(request, "todos/item_update.html", context)
